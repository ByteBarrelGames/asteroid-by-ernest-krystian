﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidSpawner : MonoBehaviour
{
    private GameObject asteroidContainer;

    public void SpawnAsteroids(int asteroidsAmount)
    {
        if (asteroidContainer == null)
        {
            asteroidContainer = new GameObject("Asteroids");
            asteroidContainer.transform.position = Vector3.zero;
            GameController.main.AsteroidsContainer = asteroidContainer;

        }
        float cammeraWidth = GameController.main.CammeraWidth;
        float cammeraHeight = GameController.main.CammeraHeight;
        float radiusGeneration = cammeraWidth > cammeraHeight ? cammeraHeight : cammeraWidth;
        float radiusOffset = 10;
        for (int i = 0; i < asteroidsAmount; i++)
        {
            Vector2 randomPos = new Vector2();
            randomPos = Random.insideUnitCircle.normalized * Random.Range(radiusGeneration + radiusOffset, radiusGeneration + radiusOffset);
            float randomQuat = Random.Range(0, 360);
            Asteroid instantiatedAsteroid = Instantiate(GameController.main.bigAsteroidPrefab, randomPos, Quaternion.Euler(new Vector3(0, 0, randomQuat)), asteroidContainer.transform);
            instantiatedAsteroid.IsNew = true;
            instantiatedAsteroid.LastObjectDirection = instantiatedAsteroid.transform.position;
            instantiatedAsteroid.SetAsteroidState(new BigAsteroid(instantiatedAsteroid));
            instantiatedAsteroid.MovementDirection = -instantiatedAsteroid.transform.position.normalized;

        }
    }
}
