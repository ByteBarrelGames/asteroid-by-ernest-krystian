﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GUIController : MonoBehaviour {
    [Header("Main Menu panel elements")]
    public GameObject mainMenuPanel;
    public Text highScoreTextMainMenu;
    [Space(10)]
    [Header("HUD panel elements")]
    public GameObject HUDPanel;
    public Text scoreTextHUD;
    public Image lifeImageHUD;
    public Sprite[] lifeGUIRepresentation;
    [Space(10)]
    [Header("Game Over panel elements")]
    public GameObject gameOverPanel;
    public Text scoreTextGameOver;
    public Text highScoreTextGameOver;
    [Space(10)]
    [Header("Pause panel elements")]
    public GameObject pausePanel;
    public Text scoreTextPause;
    
    private void Update ()
    {
        if (GameController.main.GameHasStart)
        {
            scoreTextHUD.text = "Score " + GameController.gameScore.ToString();
            lifeImageHUD.sprite = lifeGUIRepresentation[GameController.main.PlayerActualLife - 1];
        }
        if (GameController.main.GameIsOver && !gameOverPanel.activeInHierarchy)
        {
            HUDPanel.SetActive(false);
            scoreTextGameOver.text = "Game Score " + GameController.gameScore.ToString();
            highScoreTextGameOver.text = "High Score " + GameController.highScore.ToString();
            gameOverPanel.SetActive(true);
        }
        if (pausePanel.activeInHierarchy)
        {
            scoreTextPause.text = "Game Score " + GameController.gameScore.ToString();
        }
        if (mainMenuPanel.activeInHierarchy)
        {
            highScoreTextMainMenu.text = "High Score " + GameController.highScore.ToString();
        }
    }

    public void StartGame()
    {
        GameController.main.GameStart();
    }

    public void BackToMenu()
    {
        GameController.main.MainMenuInit();
    }

    public void Pause()
    {
        Time.timeScale = 0;
    }

    public void UnPause()
    {
        Time.timeScale = 1;
    }

    public void Quit()
    {

#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        System.Diagnostics.Process.GetCurrentProcess().Kill();
#endif
    }
}
