﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class GameController : MonoBehaviour
{
    public static GameController main;
    [Header("Main settings")]
    [Space(10)]
    [Header("Player")]
    public float maxPlayerSpeed;
    public float maxPlayerTorque;
    public float playerTorqueAcceleration;
    public float playerSpeedAcceleration;
    public int playerMaxLifes;
    public AudioClip playerRespawnSound;
    [Space(10)]

    [Header("Bullet")]
    public Bullet bulletPrefab;
    public float bulletSpeed;
    public float bulletAcceleration;
    public float bulletMaxLifetime;
    [Space(10)]

    [Header("Asteroids")]
    public Asteroid bigAsteroidPrefab;
    public Asteroid mediumAsteroidPrefab;
    public Asteroid smallAsteroidPrefab;
    public AsteroidSpawner asteroidSpawner;
    public float waveColdown;
    public int initialWaveAsteroidsAmount;
    [Space(10)]

    [Header("FX")]
    public GameObject explosionPrefab;
    public AudioClip playerExplosionSound;
    public AudioClip asteroidExplosionSound;

    public GameObject ExplosionContainer { get; set; }
    public float CammeraHeight { get; set; }
    public float CammeraWidth { get; set; }
    public GameObject PlayerKilled { get; set; }
    public int PlayerActualLife { get; set; }
    public bool GameHasStart { get; set; }
    public bool GameIsOver { get; set; }
    public GameObject AsteroidsContainer { get; set; }

    static public List<IMoveable> objectsToMove = new List<IMoveable>();
    static public List<IControllable> objectsToControl = new List<IControllable>();
    static public List<IShootable> shootingObjects = new List<IShootable>();
    static public List<IDamageable> damagableObjects = new List<IDamageable>();
    static public int gameScore;
    static public int highScore;
    private string playerPrefsHighScore = "highscore";
    private CorutineValues actualWaveValues;
    private AudioSource gameControllerAudioSource;
    private bool asteroidsWasSpawned;

    private void Awake()
    {
        if (main != null)
        {
            Destroy(gameObject);
        }
        else
        {
            main = this;
            DontDestroyOnLoad(this);
        }
    }

    void Start()
    {
        GetCammeraDimention();
        highScore = PlayerPrefs.GetInt(playerPrefsHighScore);
        actualWaveValues = new CorutineValues(waveColdown, initialWaveAsteroidsAmount);
        gameControllerAudioSource = GetComponent<AudioSource>();
        ExplosionContainer = new GameObject("explosionsContainer");
    }

    void Update()
    {
        if (PlayerKilled == null && !GameIsOver && GameHasStart)
        {
            foreach (IDamageable objectToCheck in objectsToControl)
            {
                if (objectToCheck.GetKilled())
                {
                    PlayerActualLife = objectToCheck.GetLifes();
                    PlayerKilled = ((MonoBehaviour)objectToCheck).gameObject;
                    PlayerKilled.SetActive(false);               
                    if (PlayerActualLife != 0)
                    {
                        ResetMap();
                        StartCoroutine("WaitForRespawn", actualWaveValues);
                        return;
                    }
                    GameOver();
                }
            }
            if(AsteroidsContainer!=null && AsteroidsContainer.transform.childCount == 0 && asteroidsWasSpawned)
            {
                Debug.Log("error");
                actualWaveValues.amount++;
                StartCoroutine("WaitForRespawn", actualWaveValues);
                asteroidsWasSpawned = false;
            }
        }
    }

    public void GameStart()
    {
        actualWaveValues = new CorutineValues(waveColdown, initialWaveAsteroidsAmount);
        PlayerActualLife = playerMaxLifes;
        gameScore = 0;
        InitializeControllableObjects();
        InitializeDamageableObjects();
        ResetMap();
        StartCoroutine("WaitForRespawn", actualWaveValues);
        GameHasStart = true;
        GameIsOver = false;
        foreach (Transform explosion in ExplosionContainer.transform)
        {
            Destroy(explosion.gameObject);
        }
    }

    public void MainMenuInit()
    {
        ResetMap();
        GameIsOver = false;
    }

    public void GameOver()
    {
        if (highScore < gameScore)
        {
            highScore = gameScore;
            PlayerPrefs.SetInt(playerPrefsHighScore, highScore);
        }
        PlayerKilled = null;
        GameIsOver = true;
        GameHasStart = false;
    }

    public void InitializeControllableObjects()
    {

        foreach (IControllable objToInit in objectsToControl)
        {
            objToInit.SetSpeedAcc(playerSpeedAcceleration);
            objToInit.SetTorqueAcc(playerTorqueAcceleration);
            objToInit.SetMaxSpeed(maxPlayerSpeed);
            objToInit.SetMaxTorque(maxPlayerTorque);
            objToInit.Activate(true);
            ((MonoBehaviour)objToInit).transform.position = Vector3.zero;
        }
    }

    public void InitializeDamageableObjects()
    {

        foreach (IDamageable objToInit in objectsToControl)
        {
            objToInit.SetLifes(playerMaxLifes);
        }
    }

    private void GetCammeraDimention()
    {
        CammeraHeight = Camera.main.orthographicSize * 2.0f;
        CammeraWidth = CammeraHeight * Screen.width / Screen.height;
    }

    public void ResetMap()
    {
        foreach (IMoveable movable in objectsToMove)
        {
            if (!(movable is IControllable))
            {
                Destroy(((MonoBehaviour)movable).gameObject);
            }
            StopAllCoroutines();
        }
        asteroidsWasSpawned = false;

    }

    private void NextWave(int asteroidAmount)
    {
        actualWaveValues = new CorutineValues(waveColdown, asteroidAmount);
        StartCoroutine("WaitForRespawn", actualWaveValues);
    }

    IEnumerator WaitForRespawn(CorutineValues values)
    {
        if (PlayerKilled != null)
        {
            PlayerKilled.GetComponent<IDamageable>().SetKilled(false);
        }
        yield return new WaitForSeconds(values.wait);
        asteroidSpawner.SpawnAsteroids(values.amount);
        asteroidsWasSpawned = true;
        if (PlayerKilled != null)
        {
            PlayerKilled.SetActive(true);
            gameControllerAudioSource.PlayOneShot(playerRespawnSound);
        }
        PlayerKilled = null;
    }

    struct CorutineValues
    {
        public float wait;
        public int amount;

        public CorutineValues(float wait, int amount)
        {
            this.wait = wait;
            this.amount = amount;
        }
    }
}
