﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveObjectController : MonoBehaviour
{
    private void FixedUpdate()
    {
        foreach (IMoveable objectToMove in GameController.objectsToMove)
        {
            objectToMove.Move();
            objectToMove.Rotate();
            GetObjectInView(objectToMove);
        }
    }

    private void GetObjectInView(IMoveable moveable)
    {

        GameObject objectToCheck = ((MonoBehaviour)moveable).gameObject;
        Renderer objectToCheckRenderer = objectToCheck.GetComponent<Renderer>();
        Vector2 half = new Vector2(Screen.width / 2, Screen.height / 2);
        float marginX = objectToCheckRenderer.bounds.extents.x;
        float marginY = objectToCheckRenderer.bounds.extents.y;
        float margin = marginX < marginY ? marginY : marginX;
        float x = Camera.main.WorldToScreenPoint(objectToCheck.transform.position).x - half.x;
        float y = Camera.main.WorldToScreenPoint(objectToCheck.transform.position).y - half.y;
        TrailRenderer[] trails = null;
        if (objectToCheck.GetComponentsInChildren<TrailRenderer>().Length > 0)
        {
            trails = objectToCheck.GetComponentsInChildren<TrailRenderer>();
        }
        if (x >= half.x + margin)
        {
            if (ObjectIsNewAsteroid(objectToCheck))
            {
                objectToCheck.GetComponent<Asteroid>().WasWrapped = true;
                return;
            }

            objectToCheck.transform.position = new Vector2(-objectToCheck.transform.position.x + margin, objectToCheck.transform.position.y);
            ClearTrails(trails);
        }
        else if (x <= -half.x - margin)
        {
            if (ObjectIsNewAsteroid(objectToCheck))
            {
                objectToCheck.GetComponent<Asteroid>().WasWrapped = true;
                return;
            }

            objectToCheck.transform.position = new Vector2(-objectToCheck.transform.position.x - margin, objectToCheck.transform.position.y);
            ClearTrails(trails);
        }
        if (y >= half.y + margin)
        {
            if (ObjectIsNewAsteroid(objectToCheck))
            {
                objectToCheck.GetComponent<Asteroid>().WasWrapped = true;
                return;
            }

            objectToCheck.transform.position = new Vector2(objectToCheck.transform.position.x, -objectToCheck.transform.position.y + margin);
            ClearTrails(trails);
        }
        else if (y <= -half.y - margin)
        {
            if (ObjectIsNewAsteroid(objectToCheck))
            {
                objectToCheck.GetComponent<Asteroid>().WasWrapped = true;
                return;
            }

            objectToCheck.transform.position = new Vector2(objectToCheck.transform.position.x, -objectToCheck.transform.position.y - margin);
            ClearTrails(trails);

        }
        if (ObjectIsNewAsteroid(objectToCheck))
            if (objectToCheck.GetComponent<Asteroid>().WasWrapped)
                objectToCheck.GetComponent<Asteroid>().IsNew = false;

    }
    bool ObjectIsNewAsteroid(GameObject objectToCheck)
    {
        if (objectToCheck.GetComponent<Asteroid>())
            if (objectToCheck.GetComponent<Asteroid>().IsNew)
                return true;
        return false;
    }
    private void ClearTrails(TrailRenderer[] trails)
    {
        if (trails != null)
        {
            foreach (TrailRenderer trail in trails)
            {
                 trail.Clear();
            }
        }
    }
}
