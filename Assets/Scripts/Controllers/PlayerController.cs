﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public GameObject touchStart;
    public GameObject touchMove;
#if UNITY_ANDROID
    private Vector2 startPosition;
    private Vector2 direction;
    private float maxDistanceMag;
    private bool fire;
    private void Start()
    {
        fire = false;
        touchStart.SetActive(false);
        touchMove.SetActive(false);
        maxDistanceMag = Screen.width * Camera.main.orthographicSize;
        Input.multiTouchEnabled = true;
    }

#endif
    private void Update()
    {
        GetShootInput();
#if UNITY_ANDROID
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            switch (touch.phase)
            {
                case TouchPhase.Began:
                    startPosition = touch.position;
                    touchStart.transform.position = startPosition;
                    touchStart.SetActive(true);
                    break;

                case TouchPhase.Moved:
                    direction = touch.position - startPosition;
                    touchMove.transform.position = touch.position;
                    touchMove.SetActive(true);
                    break;

                case TouchPhase.Ended:
                    direction = Vector2.zero;
                    touchStart.SetActive(false);
                    touchMove.SetActive(false);
                    break;
            }
        }
        if(Input.touchCount > 1)
        {
            Touch touch = Input.GetTouch(1);
            if(touch.phase == TouchPhase.Ended)
            {
                fire = true;
            }
        }
#endif
    }

    private void FixedUpdate()
    {
        foreach (IControllable controlable in GameController.objectsToControl)
        {
            controlable.SetSpeed(GetAccelerationInput());
            controlable.SetTorque(GetTorqueInput());
        }

    }

    private float GetAccelerationInput()
    {
#if !UNITY_ANDROID
        return Input.GetAxis("Vertical");
#else
        return direction.sqrMagnitude / maxDistanceMag;
#endif
    }

    private float GetTorqueInput()
    {
#if !UNITY_ANDROID
        return Input.GetAxis("Horizontal");
#else
        return direction.normalized.x;
#endif
    }

    private void GetShootInput()
    {
#if !UNITY_ANDROID
        if (Input.GetKeyDown(KeyCode.Space))
        {
#else
        if (fire)
        {
#endif
            foreach (IShootable shootObject in GameController.shootingObjects)
            {
                shootObject.Shoot(GameController.main.bulletPrefab);
            }
#if UNITY_ANDROID
            fire = false;
#endif
        }
    }

}
