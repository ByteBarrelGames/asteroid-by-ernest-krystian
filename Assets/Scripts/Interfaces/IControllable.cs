﻿
public interface IControllable: IMoveable
{
    void SetSpeed(float speed);
    void SetTorque(float torque);
    void SetTorqueAcc(float torqueAcc);
    void SetSpeedAcc(float speedAcc);
    void SetMaxSpeed(float maxSpeed);
    void SetMaxTorque(float maxTorque);
    void Activate(bool active);
}
