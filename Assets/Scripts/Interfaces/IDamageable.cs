﻿public interface IDamageable {

    void MakeDamage();
    int GetLifes();
    void SetLifes(int lifes);
    bool GetKilled();
    void SetKilled(bool killed);
}
