﻿using UnityEngine;
public interface IMoveable {

    void Move();
    void Rotate();
}
