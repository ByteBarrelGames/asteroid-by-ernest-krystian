﻿public interface IShootable{

    void Shoot(Bullet bullet);

}
