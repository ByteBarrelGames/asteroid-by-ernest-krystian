﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Asteroid : MonoBehaviour, IMoveable, IKillable
{
    public float speed;
    public float rotation;
    public float acceleration;
    public int score;
    public Vector2 LastObjectDirection { get; set; }
    public Vector3 MovementDirection { get; set; }
    public float RotationDirection { get; set; }
    public bool IsNew { get; set; }
    public bool WasWrapped { get; set; }
    private Rigidbody2D m_rigidbody2D;
    private AsteroidState currentState;


    private void Awake()
    {
        GameController.objectsToMove.Add(this);
        IsNew = true;
        WasWrapped = false;
        m_rigidbody2D = GetComponent<Rigidbody2D>();
        m_rigidbody2D.gravityScale = 0;
    }

    public void Move()
    {
        m_rigidbody2D.AddForce(MovementDirection * acceleration);
        m_rigidbody2D.velocity = Vector3.ClampMagnitude(m_rigidbody2D.velocity, speed);
    }

    public void Rotate()
    {
        m_rigidbody2D.AddTorque(RotationDirection * acceleration);
        m_rigidbody2D.angularVelocity = Mathf.Clamp(m_rigidbody2D.angularVelocity, -rotation, rotation);
    }

    public void Kill()
    {
        GameObject explosionPrefab = GameController.main.explosionPrefab;
        explosionPrefab.GetComponent<AudioSource>().clip = GameController.main.asteroidExplosionSound;
        Instantiate(explosionPrefab, transform.position, Quaternion.identity, GameController.main.ExplosionContainer.transform);
        GameController.gameScore += score;
        currentState.StateRoutine();
    }

    public void SetAsteroidState(AsteroidState state)
    {
        if (currentState == null) currentState = state;
        currentState.OnStateEnter(LastObjectDirection);
    }

    private void OnDestroy()
    {
        GameController.objectsToMove.Remove(this);
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.GetComponent<IKillable>() != null && !collider.gameObject.GetComponent<Asteroid>())
        {
            Kill();
        }
    }
}
