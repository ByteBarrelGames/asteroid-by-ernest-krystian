﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AsteroidState {

    protected Asteroid asteroid;
    protected GameObject asteroidContainer;
    public abstract void StateRoutine();
    public virtual void OnStateEnter(Vector2 lastObjDir) { }
	public AsteroidState(Asteroid asteroid)
    {
        this.asteroid = asteroid;
        asteroidContainer = GameObject.Find("Asteroids");
    }
}
