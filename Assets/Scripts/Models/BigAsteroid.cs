﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BigAsteroid : AsteroidState {

    public BigAsteroid(Asteroid asteroid) : base(asteroid)
    {
    }

    public override void OnStateEnter(Vector2 lastObjectDirection)
    {
        Vector2 direction = new Vector2
        {
            x = Random.Range(-5, 5),
            y = Random.Range(-5, 5)
        };
        asteroid.LastObjectDirection =  direction;
        asteroid.MovementDirection = direction;
        asteroid.RotationDirection = direction.normalized.y;
    }

    public override void StateRoutine()
    {
        Asteroid asteroid1 = GameObject.Instantiate(GameController.main.mediumAsteroidPrefab, asteroid.transform.position, asteroid.transform.rotation, asteroidContainer.transform);
        Asteroid asteroid2 = GameObject.Instantiate(GameController.main.mediumAsteroidPrefab, asteroid.transform.position, asteroid.transform.rotation, asteroidContainer.transform);
        asteroid1.LastObjectDirection = asteroid.MovementDirection;
        asteroid2.LastObjectDirection = -asteroid.MovementDirection;
        asteroid1.SetAsteroidState(new MediumAsteroid(asteroid1));
        asteroid2.SetAsteroidState(new MediumAsteroid(asteroid2));
        asteroid2.IsNew = false;
        asteroid1.IsNew = false;
        GameObject.Destroy(asteroid.gameObject);

    }
}
