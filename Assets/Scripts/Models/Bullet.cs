﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour, IMoveable, IKillable
{
    private float speed;
    private float acceleration;
    private float bulletMaxLifetime;
    private float actualLifeTime;
    private Rigidbody2D m_rigidbody2D;

    void Start()
    {
        actualLifeTime = 0;
        GameController.objectsToMove.Add(this);
        speed = GameController.main.bulletSpeed;
        bulletMaxLifetime = GameController.main.bulletMaxLifetime;
        acceleration = GameController.main.bulletAcceleration;
        m_rigidbody2D = GetComponent<Rigidbody2D>();
        m_rigidbody2D.gravityScale = 0;
    }

    public void Move()
    {
        m_rigidbody2D.AddForce(transform.up * acceleration);
        m_rigidbody2D.velocity = Vector3.ClampMagnitude(m_rigidbody2D.velocity, speed);
        actualLifeTime += Time.fixedDeltaTime;
        if (actualLifeTime > bulletMaxLifetime)
        {
            Destroy(this.gameObject);
        }
    }

    public void Rotate()
    {
    }

    private void OnDestroy()
    {
        GameController.objectsToMove.Remove(this);
    }

    public void Kill()
    {
        Destroy(this.gameObject);
    }


    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.GetComponent<IKillable>() != null && !collider.gameObject.GetComponent<Bullet>())
        {
            Kill();
        }
    }
}
