﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MediumAsteroid : AsteroidState {

    public MediumAsteroid(Asteroid asteroid) : base(asteroid){}
    public override void OnStateEnter(Vector2 lastObjDir)
    {
        Vector2 direction = new Vector2
        {
            x = Random.Range(-lastObjDir.x, 0), 
            y = Random.Range(0, lastObjDir.y)
        };
        asteroid.MovementDirection = direction;
        asteroid.RotationDirection = direction.normalized.y;
    }
    public override void StateRoutine()
    {
        Asteroid asteroid1 = GameObject.Instantiate(GameController.main.smallAsteroidPrefab, asteroid.transform.position, asteroid.transform.rotation, asteroidContainer.transform);
        Asteroid asteroid2 = GameObject.Instantiate(GameController.main.smallAsteroidPrefab, asteroid.transform.position, asteroid.transform.rotation, asteroidContainer.transform);
        asteroid1.LastObjectDirection = asteroid.MovementDirection;
        asteroid2.LastObjectDirection = -asteroid.MovementDirection;
        asteroid1.SetAsteroidState(new SmallAsteroid(asteroid1));
        asteroid2.SetAsteroidState(new SmallAsteroid(asteroid2));
        asteroid2.IsNew = false;
        asteroid1.IsNew = false;
        GameObject.Destroy(asteroid.gameObject);
    }
}
