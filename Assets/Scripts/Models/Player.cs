﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(SpriteRenderer))]
[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(PolygonCollider2D))]
[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(AudioSource))]
public class Player : MonoBehaviour, IControllable, IDamageable, IKillable
{
    public AudioClip shipEngineSound;
    private int lifes;
    private float speed;
    private float torque;
    private float maxTorque;
    private float maxSpeed;
    private float torqueAcceleration;
    private float speedAcceleration;
    private Rigidbody2D m_rigidbody2D;
    private bool killed;
    private AudioSource shipAudioSource;
    private Animator shipAnimator;

    private void Awake()
    {
        transform.position = Vector3.zero;
        m_rigidbody2D = GetComponent<Rigidbody2D>();
        m_rigidbody2D.gravityScale = 0;
        GameController.objectsToControl.Add(this);
        GameController.objectsToMove.Add(this);
        GameController.damagableObjects.Add(this);
        gameObject.SetActive(false);
        shipAudioSource = GetComponent<AudioSource>();
        shipAudioSource.clip = shipEngineSound;
        shipAnimator = GetComponent<Animator>();

    }
    private void OnEnable()
    {
        shipAudioSource.Play();
    }
    private void OnDestroy()
    {
        GameController.objectsToControl.Remove(this);
    }
    public void SetSpeed(float speed)
    {
        this.speed = speed;
    }

    public void SetTorque(float torque)
    {
        this.torque = torque;
    }

    public void Move()
    {
        m_rigidbody2D.AddForce(transform.up * speed * speedAcceleration);
        m_rigidbody2D.velocity = Vector3.ClampMagnitude(m_rigidbody2D.velocity, maxSpeed);
        shipAudioSource.pitch = 1 + Mathf.Abs(speed);
        shipAnimator.SetFloat("speed", m_rigidbody2D.velocity.magnitude/maxSpeed);

    }

    public void Rotate()
    {
        m_rigidbody2D.AddTorque(-torque * torqueAcceleration);
        m_rigidbody2D.angularVelocity = Mathf.Clamp(m_rigidbody2D.angularVelocity, -maxTorque, maxTorque);
    }

    public void MakeDamage()
    {
        lifes--;
        Kill();
    }

    public int GetLifes()
    {
        return lifes;
    }

    public void SetLifes(int lifes)
    {
        this.lifes = lifes;
    }

    public bool GetKilled()
    {
        return killed;
    }

    public void SetKilled(bool killed)
    {
        this.killed = killed;
    }

    public void Kill()
    {
        GameObject explosionPrefab = GameController.main.explosionPrefab;
        explosionPrefab.GetComponent<AudioSource>().clip = GameController.main.playerExplosionSound;
        Instantiate(explosionPrefab, transform.position, Quaternion.identity, GameController.main.ExplosionContainer.transform);
        transform.position = Vector3.zero;
        transform.rotation = Quaternion.Euler(Vector3.zero);
        killed = true;
    }

    public void SetTorqueAcc(float torqueAcc)
    {
        this.torqueAcceleration = torqueAcc;
    }

    public void SetSpeedAcc(float speedAcc)
    {
        this.speedAcceleration = speedAcc;
    }

    public void SetMaxSpeed(float maxSpeed)
    {
        this.maxSpeed = maxSpeed;
    }

    public void SetMaxTorque(float maxTorque)
    {
        this.maxTorque = maxTorque;
    }

    public void Activate(bool active)
    {

        gameObject.SetActive(active);
        killed = false;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<IKillable>() != null)
        {
            MakeDamage();
        }
    }


}
