﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmallAsteroid : AsteroidState
{
    public SmallAsteroid(Asteroid asteroid) : base(asteroid)
    {
    }

    public override void OnStateEnter(Vector2 lastObjectDirection)
    {
        Vector2 direction = new Vector2
        {
            x = -lastObjectDirection.x,
            y = Random.Range(0, lastObjectDirection.y)
        };
        asteroid.MovementDirection = direction;
        asteroid.RotationDirection = direction.normalized.y;
    }

    public override void StateRoutine()
    {
        GameObject.Destroy(asteroid.gameObject);
    }
}
