﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turret : MonoBehaviour, IShootable
{
    private Rigidbody2D shipRigidbody2D;
    private void Awake()
    {
        GameController.shootingObjects.Add(this);
        shipRigidbody2D = transform.parent.GetComponent<Rigidbody2D>();
    }

    public void Shoot(Bullet bullet)
    {
        if (this.gameObject.activeInHierarchy)
        {
            Rigidbody2D bulletRigidbody2D = Instantiate(bullet, transform.position, transform.rotation).GetComponent<Rigidbody2D>();
            bulletRigidbody2D.velocity += shipRigidbody2D.velocity;

        }


    }
}
